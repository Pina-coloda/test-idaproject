import "./style.scss";

import "./libs/grid-layout-polyfill.min.js";

import 'jquery-validation'

$('.menu__icon').on('click', function () {
    $(this).toggleClass('menu__icon-active');
    $('.menu__list').fadeToggle('slow');
});

$('.cards_num').on('keyup', function () {
    if($(this).val().match(/^\d{4}$/)){
        $(this).next('.cards_num').focus();
    }
});

jQuery.validator.addMethod("accept", function(value, element, param) {
    return value.match(new RegExp("." + param + "$"));
});

$('#form').validate({
    rules: {
        card_1: {
            required: true,
            minlength: 4,
            maxlength: 4,
            digits: true
        },
        card_2: {
            required: true,
            minlength: 4,
            maxlength: 4,
            digits: true
        },
        card_3: {
            required: true,
            minlength: 4,
            maxlength: 4,
            digits: true
        },
        card_4: {
            required: true,
            minlength: 4,
            maxlength: 4,
            digits: true
        },
        card_month: {
            required: true
        },
        card_year: {
            required: true
        },
        card_name: {
            required: true,
            minlength: 4,
            accept: "[a-zA-Z]+"
        },
        card_cvc: {
            required: true,
            minlength: 3,
            maxlength: 3,
            digits: true
        },
    }
});